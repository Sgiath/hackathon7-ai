"""Demo showing Reinforcement Learning for AI Hackathon

Original Google Colaboratory document can be found here https://goo.gl/9VXUfC

Author:
    Filip Vavera <FilipVavera@sgiath.net> (https://sgiath.net/)
"""
import os
import sys
from typing import List, Dict

import gym
import numpy as np
import tensorflow as tf
import tflearn
from tflearn.layers.core import input_data, fully_connected, dropout
from tflearn.layers.estimator import regression


def population(env: gym.Env, model: tflearn.DNN = None, games: int = 1000, score_threshold: int = 50):
    """Play game multiple times and create population for training

    Arguments:
        env: OpenAI Gym environment to train on
        model: DNN network model (if not provided, random action is taken)
        games: number of games to play
        score_threshold: minimum score to accept the game

    Returns:
        List of played game (contains observations and actions)
    """
    # [observations, moves]
    training_data: List[List[np.ndarray, List[int]]] = []

    # Scores from all games
    scores: List[float] = []
    # All scores from games with good score
    accepted_scores: List[float] = []

    # Run individual games
    for _ in range(games):
        # Actual game score
        score: float = 0.0

        # Game history (if final score is greater then tr
        game_memory: List[List[np.ndarray, int]] = []
        # Actual observation
        observation: np.ndarray = env.reset()
        # Is the game done?
        done: bool = False

        # Run game until it is finished
        while not done:
            # If gathering data without model take random action else predict action from our model
            if model is None:
                action: int = env.action_space.sample()
            else:
                action: int = np.argmax(model.predict(observation.reshape(-1, len(observation), 1))[0])

            # Append observation and taken action to game memory
            game_memory.append([observation, action])
            # Take step in game with our action
            observation, reward, done, info = env.step(action)
            # Add score
            score += reward

        # Filter low score games
        if score >= score_threshold:
            accepted_scores.append(score)
            for data in game_memory:
                # convert to one-hot (this is the output layer for our neural network)
                if data[1] == 1:
                    output = [0, 1]
                else:
                    output = [1, 0]
                training_data.append([data[0], output])

        scores.append(score)

    print('Average score: {}'.format(np.mean(scores)))
    print('Average accepted score: {}'.format(np.mean(accepted_scores)))
    print('Median score for accepted scores: {}'.format(np.median(accepted_scores)))
    print('Number of accepted games from {}: {}'.format(games, len(accepted_scores)))
    u, uc = np.unique(accepted_scores, return_counts=True)
    counts: Dict[float, int] = {}
    for i in range(u.size):
        counts[u[i]] = uc[i]
    print(counts)

    return training_data


def neural_network_model(input_size: int, lr: float = 1e-3) -> tflearn.DNN:
    """Create new DNN network

    Arguments:
        input_size: size of the input layer
        lr: learning rate

    Returns:
        New DNN network
    """
    # Our input layer
    network = input_data(shape=[None, input_size, 1], name='input')

    # Hidden layers
    network = fully_connected(network, 128, activation='relu')
    network = dropout(network, 0.8)

    network = fully_connected(network, 256, activation='relu')
    network = dropout(network, 0.8)

    network = fully_connected(network, 512, activation='relu')
    network = dropout(network, 0.8)

    network = fully_connected(network, 256, activation='relu')
    network = dropout(network, 0.8)

    network = fully_connected(network, 128, activation='relu')
    network = dropout(network, 0.8)

    # Output layer
    network = fully_connected(network, 2, activation='softmax')
    network = regression(network, optimizer='adam', learning_rate=lr, loss='categorical_crossentropy', name='targets')

    # Export our DNN model
    return tflearn.DNN(network, tensorboard_dir='log', tensorboard_verbose=3)


def train_model(training_data, model: tflearn.DNN = None) -> tflearn.DNN:
    """Take training data and optionally DNN network and train it

    Arguments:
        training_data: data to train on
        model: DNN network (if not provided new DNN network is created)

    Returns:
        Trained DNN network
    """
    # Observations (reshape for our input layer)
    x = np.array([i[0] for i in training_data]).reshape(-1, len(training_data[0][0]), 1)
    # Actions
    y = [i[1] for i in training_data]

    # Create new model if none was provided
    if model is None:
        model = neural_network_model(input_size=len(x[0]))  # Input size is size of the observations

    # Our learning function - you can experiment with params
    model.fit({'input': x}, {'targets': y}, validation_set=0.2, n_epoch=5, show_metric=True, run_id='openai_learning')
    return model


def run_game(env: gym.Env, model: tflearn.DNN = None, render: bool = True) -> float:
    """Run the basic game played by the DNN

    Arguments:
        env: OpenAI Gym environment
        model: DNN network model
        render: should game be rendered?

    Returns:
        Game score
    """
    # Game score
    score: float = 0.0
    # Is game finished
    done: bool = False
    # State observation
    observation: np.ndarray = env.reset()

    # Game cycle
    while not done:
        if model is None:
            # If model is not provided take random action
            action: int = env.action_space.sample()
        else:
            # Gather action from our DNN network based on the observation
            action: int = np.argmax(model.predict(observation.reshape(-1, len(observation), 1))[0])
        # Execute game step with our action
        observation, reward, done, info = env.step(action)

        # Render game if should be rendered
        if render:
            env.render()
        # Update game score
        score += reward
    return score


def main() -> int:
    tflearn.config.init_graph(gpu_memory_fraction=0.8)
    model_file: str = os.path.join(os.path.dirname(__file__), 'model', 'CartPole')
    env: gym.Env = gym.make('CartPole-v0')

    if os.path.isfile('{}.index'.format(model_file)) and input('Load saved model? [y/n] ') == 'y':
        # Create default model and load weights from file
        model: tflearn.DNN = neural_network_model(env.observation_space.shape[0])
        model.load(model_file)
    else:
        # Train new model from random population
        model: tflearn.DNN = train_model(population(env, games=20000))

    if input('Train model? [y/n] ') == 'y':
        for i in range(10):
            model = train_model(population(env, model, score_threshold=150), model)
    print()

    if input('Run off-screen games? [y/n] ') == 'y':
        print('Running 1000 games off-screen to compute average...')
        scores = []
        for _ in range(1000):
            scores.append(run_game(env, model, False))
        print('Average score for 1000 games: {}'.format(np.mean(scores)))
    print()

    if input('Run random games? [y/n] ') == 'y':
        print('Run random games...')
        for i in range(10):
            print('Random game {}, Score {}'.format(i + 1, run_game(env)))
    print()

    if input('Run DNN games? [y/n] ') == 'y':
        print('Run games with DNN...')
        for i in range(10):
            print('DNN game {}, Score: {}'.format(i + 1, run_game(env, model)))
    print()
    env.close()

    if input('Save model? [y/n] ') == 'y':
        model.save(model_file)

    return 0


if __name__ == '__main__':
    with tf.device('/device:GPU:0'):
        sys.exit(main())
