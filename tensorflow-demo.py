import sys

import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import gym


class CartPoleNetwork(object):
    def __init__(self, lr: float, s_size: int, a_size: int):
        self.s_size = s_size
        self.a_size = a_size

        # These lines established the feed-forward part of the network. The agent takes a state and produces an action.
        self.state_in = tf.placeholder(dtype=tf.float32, shape=[None, s_size])

        hidden = slim.fully_connected(self.state_in, 8)
        # hidden = slim.dropout(hidden, 0.8)

        hidden = slim.fully_connected(hidden, 8)
        # hidden = slim.dropout(hidden, 0.8)

        hidden = slim.fully_connected(hidden, 8)
        # hidden = slim.dropout(hidden, 0.8)

        hidden = slim.fully_connected(hidden, 8)
        # hidden = slim.dropout(hidden, 0.8)

        # hidden = slim.fully_connected(hidden, 8)
        # hidden = slim.dropout(hidden, 0.8)

        self.output = slim.fully_connected(hidden, a_size, activation_fn=tf.nn.softmax)
        self.chosen_action = tf.argmax(self.output, 1)

        # The next six lines establish the training procedure. We feed the reward and chosen action into the network
        # to compute the loss, and use it to update the network.
        self.reward_holder = tf.placeholder(shape=[None], dtype=tf.float32)
        self.action_holder = tf.placeholder(shape=[None], dtype=tf.int32)

        self.indexes = tf.range(0, tf.shape(self.output)[0]) * tf.shape(self.output)[1] + self.action_holder
        self.responsible_outputs = tf.gather(tf.reshape(self.output, [-1]), self.indexes)

        self.loss = -tf.reduce_mean(tf.log(self.responsible_outputs) * self.reward_holder)

        tvars = tf.trainable_variables()
        self.gradient_holders = []
        for idx, var in enumerate(tvars):
            placeholder = tf.placeholder(tf.float32, name=str(idx) + '_holder')
            self.gradient_holders.append(placeholder)

        self.gradients = tf.gradients(self.loss, tvars)

        optimizer = tf.train.AdamOptimizer(learning_rate=lr)
        self.update_batch = optimizer.apply_gradients(zip(self.gradient_holders, tvars))


def discount_rewards(r: np.ndarray, gamma: float = 0.99):
    """ take 1D float array of rewards and compute discounted reward """
    discounted_r = np.zeros_like(r)
    running_add = 0
    for t in reversed(range(r.size)):
        running_add = running_add * gamma + r[t]
        discounted_r[t] = running_add
    return discounted_r


def main():
    tf.reset_default_graph()

    env = gym.make('CartPole-v0')
    net = CartPoleNetwork(lr=1e-2, s_size=4, a_size=2)  # Load the agent.

    total_episodes = 5000
    update_frequency = 5

    init = tf.global_variables_initializer()

    sess = tf.Session()
    sess.run(init)
    total_reward = []
    total_length = []
    render = False

    grad_buffer = sess.run(tf.trainable_variables())
    for ix, grad in enumerate(grad_buffer):
        grad_buffer[ix] = grad * 0

    for i in range(total_episodes):
        s = env.reset()
        running_reward = 0
        ep_history = []

        for j in range(300):
            # Probabilistically pick an action given our network outputs.
            a_dist = sess.run(net.output, feed_dict={net.state_in: [s]})
            a = np.random.choice(a_dist[0], p=a_dist[0])
            a = np.argmax(a_dist == a)

            s1, reward, done, _ = env.step(a)
            if render:
                env.render()
            ep_history.append([s, a, reward, s1])
            s = s1
            running_reward += reward
            if done:
                # Update the network.
                ep_history = np.array(ep_history)
                ep_history[:, 2] = discount_rewards(ep_history[:, 2])
                feed_dict = {
                    net.reward_holder: ep_history[:, 2],
                    net.action_holder: ep_history[:, 1],
                    net.state_in: np.vstack(ep_history[:, 0])
                }
                grads = sess.run(net.gradients, feed_dict=feed_dict)
                for idx, grad in enumerate(grads):
                    grad_buffer[idx] += grad

                if i % update_frequency == 0 and i != 0:
                    feed_dict = dict(zip(net.gradient_holders, grad_buffer))
                    _ = sess.run(net.update_batch, feed_dict=feed_dict)
                    for ix, grad in enumerate(grad_buffer):
                        grad_buffer[ix] = grad * 0

                total_reward.append(running_reward)
                total_length.append(j)
                break

        if i % 100 == 0:
            reward_mean = np.mean(total_reward[-100:])
            print(reward_mean)

    sess.close()
    env.close()
    return 0


if __name__ == '__main__':
    with tf.device('/device:GPU:0'):
        sys.exit(main())
